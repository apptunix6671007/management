const bcrypt = require('bcrypt');  
const { Librarian, Patron, UserBook } = require('../models/User');
const { Library } = require('../models/Catalog');  
const patronSchemaValidation =require('../Validations/patronValidations');
const librarianSchemaValidation=require('../Validations/patronValidations');
const jwt = require('jsonwebtoken');
const generateToken=require('../utils/tokens')
const Joi=require('joi');
 // Function to create a librarian
async function createLibrarian(req, res) {
    try {
        const { name, email, phoneNo, password, libraryId } = req.body;
  
        const hashedPassword = await bcrypt.hash(password, 10);

        const libraryExists = await Library.findById(libraryId);
        if (!libraryExists) {
            return res.status(400).json({ error: 'Library not found' });
        }

         
        await Librarian.create({ name, email, phoneNo, password: hashedPassword, library: libraryId });
        console.log('New librarian created successfully');

        return res.status(201).json({ message: 'Librarian created successfully',token });
    } catch (error) {
        console.error('Error creating librarian:', error.message);
        return res.status(500).json({ error: 'Internal server error' });
    }
}

 
 
 

async function registerPatron(req, res) {
    try {
        const { name, email, phoneNo, password, libraryId, libraryMembershipId } = req.body;
 
        await patronSchemaValidation.validateAsync(req.body);
// if (error) {
    
//     return res.status(400).json({ error: error.details[0].message });
// }
        if (!libraryId) {
            return res.status(400).json({ error: 'Library ID is required' });
        }
 
        const library = await Library.findById(libraryId);
        if (!library) {
            return res.status(404).json({ error: 'Library not found' });
        }

    
        const hashedPassword = await bcrypt.hash(password, 10);

        
        const newPatron = await Patron.create({
            name,
            email,
            phoneNo,
            password: hashedPassword,  
            libraryMembershipId,
            libraries: libraryId 
        });
        const token = generateToken({ _id: newPatron._id });

        console.log('New patron registered successfully:', newPatron);
         
        return res.status(201).json({ patron: newPatron, token });
    } catch (error) {
        console.error('Error registering patron:', error.message);
        return res.status(500).json({ error: 'Internal server error' });
    }
}

 


// Function to log in a patron
async function loginPatron(req, res) {
    try {
        const { email, password } = req.body;

       
        const patron = await Patron.findOne({ email });
        if (!patron) {
            return res.status(404).json({ error: 'Patron not found' });
        }

       
        const passwordMatch = await bcrypt.compare(password, patron.password);
        if (!passwordMatch) {
            return res.status(401).json({ error: 'Incorrect password' });
        }

        const token = generateToken({ _id: patron._id });
        return res.status(200).json({ message: 'Login successful',token });
    } catch (error) {
        console.error('Error logging in patron:', error.message);
        return res.status(500).json({ error: 'Internal server error' });
    }
}

 
// Controller function to view librarian profile
async function viewLibrarianProfile(req, res) {
    try {
        const {librarianId} = req.body;
 
        const librarian = await Librarian.findById(librarianId);

        if (!librarian) {
            return res.status(404).json({ error: 'Librarian not found' });
        }

        return res.status(200).json({ librarian });
    } catch (error) {
        console.error('Error fetching librarian profile:', error.message);
        return res.status(500).json({ error: 'Internal server error' });
    }
}

// Controller function to update librarian profile
async function updateLibrarianProfile(req, res) {
    try {
         
        const { name, email, password,librarianId} = req.body;

        // Fetch librarian from the database based on their ID
        let librarian = await Librarian.findById(librarianId);

        if (!librarian) {
            return res.status(404).json({ error: 'Librarian not found' });
        }

       
        librarian.name = name || librarian.name;
        librarian.email = email || librarian.email;
        password.email=password || librarian.password
       
        librarian = await librarian.save();

        return res.status(200).json({ message: 'Librarian profile updated successfully', librarian });
    } catch (error) {
        console.error('Error updating librarian profile:', error.message);
        return res.status(500).json({ error: 'Internal server error' });
    }
}


async function viewPatronsProfile(req, res) {
    try {
        
        const {patronId} = req.body;
 
        const patronProfile = await Patron.findById(patronId);

        
        res.status(200).json(patronProfile);
    } catch (error) {
        console.error('Error viewing patron profile:', error.message);
        res.status(500).json({ error: 'Internal server error' });
    }
}
 
async function updatePatronProfile(req, res) {
    try {
 
     

        
        const { name, email, password, patronId } = req.body;

        
        if (!name && !email && !password ) {
            return res.status(400).json({ error: 'At least one field is required to update' });
        }

        
        let patron = await Patron.findById(patronId);

       
        if (!patron) {
            return res.status(404).json({ error: 'Patron not found' });
        }

      
        patron.name = name || patron.name;
        patron.email = email || patron.email;
        patron.password = password || patron.password;  
        

        
        patron = await patron.save();

        
        return res.status(200).json({ message: 'Patron profile updated successfully', patron });
    }  catch (error) {
        console.error('Error updating patron profile:', error.message);
        res.status(500).json({ error: 'Internal server error' });
    }
}
async function createRoom(librarianId, patronId) {
    try {
        const room = await Room.create({ librarian: librarianId, patron: patronId });
        console.log('Room created successfully:', room);
        return room;
    } catch (error) {
        console.error('Error creating room:', error.message);
        throw error;
    }
}
 ////
 async function renewals(req, res) {
    try {
        const { userId, bookId } = req.body;

       
        const patron = await Patron.findById(userId);
        if (!patron) {
            return res.status(404).json({ error: 'Patron not found' });
        }
 
        const item = await UserBook.findOne({ userId, bookId });
        if (!item) {
            return res.status(404).json({ error: 'Borrowed item not found' });
        }

        
        const returnDate = new Date(Date.now() + 10 * 24 * 60 * 60 * 1000);

        
        item.returnDate = returnDate;
        await item.save();
 
        return res.status(200).json({ message: 'Item successfully renewed', item });
    } catch (error) {
        console.error('Error renewing item:', error.message);
        return res.status(500).json({ error: 'Internal server error' });
    }
}


 
async function returns(req, res) {
    try {
        const { userId, bookId } = req.body;

  
        const patron = await Patron.findById(userId);
        if (!patron) {
            return res.status(404).json({ error: 'Patron not found' });
        }

     
        const item = await UserBook.findOne({ userId, bookId });
        if (!item) {
            return res.status(404).json({ error: 'Borrowed item not found' });
        }
 
        if (item.status === 'returned') {
            return res.status(400).json({ error: 'Item has already been returned' });
        }

    
        item.status = 'returned';
        item.returnDate = new Date();
        await item.save();
 
        return res.status(200).json({ message: 'Item successfully returned', item });

    } catch (error) {
        console.error('Error returning item:', error.message);
        return res.status(500).json({ error: 'Internal server error' });
    }
};

 

module.exports = {renewals,returns,createRoom,updatePatronProfile, viewPatronsProfile,createLibrarian, registerPatron, loginPatron,updateLibrarianProfile,viewLibrarianProfile };
