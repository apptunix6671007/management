const { Library, Book } = require('../models/Catalog');  
const {UserBook}=require('../models/User')
// Async function to create a new library
async function createLibrary(req, res) {
    try {
        const { name } = req.body;
        const newLibrary = await Library.create({ name });
        console.log('New library created successfully:', newLibrary);
        return res.status(201).json(newLibrary);
    } catch (error) {
        console.error('Error creating library:', error.message);
        return res.status(500).json({ error: 'Internal server error' });
    }
}

// Async function to create a new book
async function createBook(req, res) {
    try {
        const { title, author, library, status } = req.body;
        
        if (!library) {
            return res.status(400).json({ error: 'Library ID is required' });
        }
        
         
        const newBook = await Book.create({
            title: title,
            author: author,
            library: library,
            status: status || 'available'  
        });

    
        return res.status(201).json(newBook);
    } catch (error) {
        // Handle any errors
        console.error('Error creating book:', error.message);
        return res.status(500).json({ error: 'Internal server error' });
    }
}

 
async function borrowBook(req, res) {
    try {
        const { userId, bookId, libraryId } = req.body;

        // Find the book by ID
        const book = await Book.findOne({ _id: bookId, library: libraryId });

       // If the book is not found or is already borrowed, return an error
if(!book) 
 return res.status(400).json({error:"book not available in particular library"}); 
 
 
        // Create a new entry in the UserBook schema
        const newUserBook = await UserBook.create({ userId, bookId, libraryId,status:'borrowed'});

        // Return a success response
        return res.status(200).json({ message: 'Book successfully borrowed', userBook: newUserBook });
    }  catch (error) {
        console.error('Error borrowing book:', error.message);
        return res.status(500).json({ error: 'Internal server error' });
    }
}

async function returnBook(req, res) {
    try {
        const { userId, bookId } = req.body;

      
        const userBook = await UserBook.findOne({ userId, bookId });

     
        if (!userBook || userBook.status !== 'borrowed') {
            return res.status(404).json({ error: 'User has not borrowed this book' });
        }

       
        const book = await Book.findById(bookId);
        if (book) {
            book.status = 'available';
            await book.save();
        }

      
        userBook.status = 'returned';
        await userBook.save();

       
        return res.status(200).json({ message: 'Book successfully returned' });
    } catch (error) {
        console.error('Error returning book:', error.message);
        return res.status(500).json({ error: 'Internal server error' });
    }
}

async function deleteBook(req, res) {
    try {
        
 const {bookId}=req.body;
        const book = await Book.findById(bookId);

       
        if (!book) {
            return res.status(404).json({ error: 'Book not found' });
        }
 
        await Book.findByIdAndDelete({ _id: bookId });

    
        return res.status(200).json({ message: 'Book successfully deleted' });
    } catch (error) {
        console.error('Error deleting book:', error.message);
        return res.status(500).json({ error: 'Internal server error' });
    }
}
async function editBook(req, res) {
    try {
 
        const { title, author, library,bookId } = req.body; // Extract updated book information from request body

        // Find the book by its ID
        let book = await Book.findById(bookId);

        // If the book is not found, return a 404 error
        if (!book) {
            return res.status(404).json({ error: 'Book not found' });
        }

        // Update the book's information with the provided values, or keep the existing values if not provided
        book.title = title || book.title;
        book.author = author || book.author;
        book.library = library || book.library;

        // Save the updated book to the database
        book = await book.save();

        // Return the updated book as the response
        return res.status(200).json({ message: 'Book successfully updated', book });
    } catch (error) {
        console.error('Error editing book:', error.message);
        return res.status(500).json({ error: 'Internal server error' });
    }
}


module.exports = { createLibrary, createBook,borrowBook,returnBook,deleteBook,editBook};
