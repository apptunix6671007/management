const express = require('express');
const router = express.Router();
const {returns,renewals,updatePatronProfile,viewPatronsProfile, createLibrarian, registerPatron, loginPatron ,updateLibrarianProfile,viewLibrarianProfile} = require('../controllers/UserControllers');

 
router.post('/librarians', createLibrarian);

router.post('/patrons/register', registerPatron);

 
router.post('/patrons/login', loginPatron);

router.put('/librarian_profile_update',updateLibrarianProfile)
router.get('/librarian_view',viewLibrarianProfile)
router.get('/patrons_profile',viewPatronsProfile)
router.get('/patrons_update',updatePatronProfile)
router.post('/renewals',renewals);
router.post('/returns',returns);
module.exports = router;
