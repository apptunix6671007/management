const express = require('express');
const router = express.Router();
const { createLibrary, createBook ,borrowBook, addBook,editBook,deleteBook,returnBook} = require('../controllers/CatalogControllers');

// Route to create a new library
router.post('/libraries', createLibrary);

// Route to create a new book
router.post('/books', createBook);
router.post('/borrow',borrowBook);
// Add a new book
 

// Edit an existing book
router.put('/books', editBook);

// Delete a book
router.delete('/books', deleteBook);

 
router.put('/booksreturn', returnBook);
module.exports = router;
