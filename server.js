const express = require("express");
const mongoose = require("mongoose");
const server = express();
server.use(express.json());
const cors = require("cors");
const swaggerUi = require("swagger-ui-express");
const YAML = require("yamljs");
const socketHandler = require('./socket');
 
// const createSwagger = require('./Swagger.js')
server.use(cors());
 
 
 
mongoose.connect("mongodb://localhost:27017/MANAGEMENT");
//importing routes files 
const CatalogRoutes=require("./routes/CatalogRoutes");
const UserRoutes=require("./routes/UserRoutes");
 
//middleware
server.use("/Catalog",CatalogRoutes);
 server.use("/Users",UserRoutes);
const port =1100;
socketHandler(server);
server.listen(port, function () {
    console.log(`Server running on port ${port}`);
  });