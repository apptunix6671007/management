const jwt = require('jsonwebtoken');

function generateToken(user) {
   
    const token = jwt.sign({ userId: user._id }, 'your_secret_key_here', { expiresIn: '3h' });
    return token;
}

module.exports = generateToken;
