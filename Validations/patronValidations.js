const Joi = require('joi'); // Import Joi for schema validation

 
const patronSchemaValidation = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().email().required(),
    phoneNo: Joi.number().required(), 
    password: Joi.string().required(),
    libraryMembershipId: Joi.string().required(),
    libraryId: Joi.string().required()  
});

module.exports = patronSchemaValidation;
