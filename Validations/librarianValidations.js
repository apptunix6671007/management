const Joi = require('joi');

// Define Joi schema for Librarian
const librarianSchemaValidation = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }).required(),
    password: Joi.string().required(),
    library: Joi.string().required()  
});

module.exports = librarianSchemaValidation;
