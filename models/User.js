const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema for librarian
const librarianSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    library: {
        type: Schema.Types.ObjectId,
        ref: 'Library',
        required: true
    }
});

// Schema for patron
const patronSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    phoneNo: {
        type: Number,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    libraryMembershipId: {
        type: String,
        required: true
    },
    libraries: {
        type: Schema.Types.ObjectId,
        ref: 'Library'
    }
});
   
const userBookSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'Patron',
        required: true
    },
    bookId: {
        type: Schema.Types.ObjectId,
        ref: 'Book',
        required: true
    },
    libraryId: {
        type: Schema.Types.ObjectId,
        ref: 'Library',
        required: true
    },
    status: {
        type: String,
        enum: [ 'borrowed', 'returned','available'],
        default: 'available'
    }
});


const UserBook = mongoose.model('UserBook', userBookSchema);

///
const roomSchema = new mongoose.Schema({
    users: [mongoose.Schema.Types.ObjectId],
   
  });
  
  const Room = mongoose.model('Room', roomSchema);
  
   
  const messageSchema = new mongoose.Schema({
    senderId: mongoose.Schema.Types.ObjectId,
    receiverId: mongoose.Schema.Types.ObjectId,
    message: String,
   
  });
  
  const Message = mongoose.model('Message', messageSchema);
  
   
////
patronSchema.index({ library: 1, email: 1 }, { unique: true });

 
patronSchema.index({ library: 1, phoneNo: 1 }, { unique: true });

patronSchema.index({ library: 1, libraryMembershipId: 1 }, { unique: true });
 
const Librarian = mongoose.model('Librarian', librarianSchema);
const Patron = mongoose.model('Patron', patronSchema);

module.exports = { Librarian, Patron,UserBook,Room,Message };
