const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema for books
const bookSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    },
    library: {
        type: Schema.Types.ObjectId,
        ref: 'Library',
        required: true
    },
    status: {
        type: String,
        enum: ['borrowed', 'returned', 'available'],
        default: 'available'
    }
});


// Schema for library
const librarySchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }
});

const roomSchema = new mongoose.Schema({
    librarian: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Librarian',
        required: true
    },
    patron: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Patron',
        required: true
    }
});

const Room = mongoose.model('Room', roomSchema);


const roommSchema = new mongoose.Schema({
    users: [mongoose.Schema.Types.ObjectId],
   
  });
  
  const Roomm = mongoose.model('Room', roommSchema);
  
  
 
const Book = mongoose.model('Book', bookSchema);
const Library = mongoose.model('Library', librarySchema);

module.exports = { Book, Library,Room,Roomm };